
class PassWinners {
  int year;
  String app;

  PassWinners({
    required this.year,
    required this.app
  });
}

List<PassWinners> apps = [
  PassWinners(
    year: 2012, 
    app: 'FNB'
  ),
  PassWinners(
    year: 2013, 
    app: 'SnapScan'
  ),
  PassWinners(
    year: 2021, 
    app: 'Edtech'
  ),
  PassWinners(
    year: 2014, 
    app: 'Live Inspect'
  ),
  PassWinners(
    year: 2016, 
    app: 'Domestly'
  ),
  PassWinners(
    year: 2017, 
    app: 'Shyft'
  ),
  PassWinners(
    year: 2018, 
    app: 'Khula ecosystem'
  ),
  PassWinners(
    year: 2019, 
    app: 'Naked Insurance'
  ),
  PassWinners(
    year: 2020, 
    app: 'EasyEquities'
  ),
  PassWinners(
    year: 2015, 
    app: 'WumDrop'
  ),
];



void main() {
  
  print('sort and print the apps by name : ');
  apps.sort((a, b) => a.app.compareTo(b.app));
  apps.forEach((element) {
    print('${element.app}');
  });

  print('' * 3);
  print('Print the winning app of 2017 and the winning app of 2018. : ');
  apps.forEach((element) => {
    if(element.year == 2017 || element.year == 2018) {
      print('${element.app}')
    }
  });

  print('' * 3);

  // Print total number of apps from the array.
  print('Length of the array: ${apps.length}');


  
}
